# Prefect handbook

<div class="tags has-addons">
  <span class="tag is-medium">April 5, 2020</span>
</div>

A beginners guide to task and workflow scheduling with Prefect. One of the reasons why one might choose Prefect over Airflow is because of it's ability to run on Windows without the workarounds like using Docker. For a full comparsion with Airflow (probably not without bias), read [Why Not Airflow?](https://medium.com/the-prefect-blog/why-not-airflow-4cfa423299c4) authored by Prefect themselves.

Most of the following instructions come from the official [Prefect Docs](https://docs.prefect.io/core/).

## Requirements

- Docker and Docker Compose (for [Prefect's server component](https://docs.prefect.io/orchestration/))
- Python (>= 3.6)
- Prefect

Note that you may want to first test things out in a virtual environment before doing a global installation.

```
python3.6 -m venv env
source env/bin/activate
pip install prefect
```

## A basic task

Once the requirements are installed, let's set up a basic example of running a `task` with Prefect.

```python
# basic.py
from Prefect import task, Flow

@task(log_stdout=True)
def basic_task:
    print("Hello World")

with Flow("Basic-Flow") as flow:
    basic_task()

flow.run()
```

<details>
  <summary>Show terminal output</summary>

```
user@machine:~$ python basic.py
[2020-04-04 23:50:37,508] INFO - prefect.FlowRunner | Beginning Flow run for 'Basic Flow'
[2020-04-04 23:50:37,512] INFO - prefect.FlowRunner | Starting flow run.
[2020-04-04 23:50:37,529] INFO - prefect.TaskRunner | Task 'basic_task': Starting task run...
[2020-04-04 23:50:37,529] INFO - prefect.TaskRunner | Hello World
[2020-04-04 23:50:37,535] INFO - prefect.TaskRunner | Task 'basic_task': finished task run for task with final state: 'Success'
[2020-04-04 23:50:37,536] INFO - prefect.FlowRunner | Flow run SUCCESS: all reference tasks succeeded
```

</details>

## Adding dependencies and parameters

Most of the time, a `task` run will depend on other `task` (the completion of the run or the resulting data). `Flow`'s capture these dependencies and are basically a representation of a collection of dependent `task`'s. The example below shows such an "advanced" flow where the `calculate_average` task is dependent upon the data returned by the `check_length` task.

Notice that this flow is run twice and it now accepts the `nums` parameter.

```python
# advance.py
from prefect import task, Flow, Parameter

@task(log_stdout=True)
def check_length(array):
    length = len(array)
    print("Number of items: {}.".format(length))
    return length

@task(log_stdout=True)
def calculate_average(array, length):
    result = sum(array) / length
    print("Calculated average: {}.".format(result))

with Flow("Advance-Flow") as flow:
    nums = Parameter("nums")
    length = check_length(nums)
    calculate_average(nums, length)

flow.run(nums=[1, 2, 3])
flow.run(nums=[6, 7, 8, 9])
```

<details>
  <summary>Show terminal output</summary>

```
user@machine:~$ python advance.py
[2020-04-05 00:53:01,705] INFO - prefect.FlowRunner | Beginning Flow run for 'Advance-Flow'
[2020-04-05 00:53:01,709] INFO - prefect.FlowRunner | Starting flow run.
[2020-04-05 00:53:01,729] INFO - prefect.TaskRunner | Task 'nums': Starting task run...
[2020-04-05 00:53:01,737] INFO - prefect.TaskRunner | Task 'nums': finished task run for task with final state: 'Success'
[2020-04-05 00:53:01,753] INFO - prefect.TaskRunner | Task 'check_length': Starting task run...
[2020-04-05 00:53:01,753] INFO - prefect.TaskRunner | Number of items: 3.
[2020-04-05 00:53:01,761] INFO - prefect.TaskRunner | Task 'check_length': finished task run for task with final state: 'Success'
[2020-04-05 00:53:01,778] INFO - prefect.TaskRunner | Task 'calculate_average': Starting task run...
[2020-04-05 00:53:01,778] INFO - prefect.TaskRunner | Calculated average: 2.0.
[2020-04-05 00:53:01,785] INFO - prefect.TaskRunner | Task 'calculate_average': finished task run for task with final state: 'Success'
[2020-04-05 00:53:01,786] INFO - prefect.FlowRunner | Flow run SUCCESS: all reference tasks succeeded
[2020-04-05 00:53:01,786] INFO - prefect.FlowRunner | Beginning Flow run for 'Advance-Flow'
[2020-04-05 00:53:01,790] INFO - prefect.FlowRunner | Starting flow run.
[2020-04-05 00:53:01,806] INFO - prefect.TaskRunner | Task 'nums': Starting task run...
[2020-04-05 00:53:01,813] INFO - prefect.TaskRunner | Task 'nums': finished task run for task with final state: 'Success'
[2020-04-05 00:53:01,831] INFO - prefect.TaskRunner | Task 'check_length': Starting task run...
[2020-04-05 00:53:01,831] INFO - prefect.TaskRunner | Number of items: 4.
[2020-04-05 00:53:01,838] INFO - prefect.TaskRunner | Task 'check_length': finished task run for task with final state: 'Success'
[2020-04-05 00:53:01,854] INFO - prefect.TaskRunner | Task 'calculate_average': Starting task run...
[2020-04-05 00:53:01,854] INFO - prefect.TaskRunner | Calculated average: 7.5.
[2020-04-05 00:53:01,860] INFO - prefect.TaskRunner | Task 'calculate_average': finished task run for task with final state: 'Success'
[2020-04-05 00:53:01,861] INFO - prefect.FlowRunner | Flow run SUCCESS: all reference tasks succeeded
```

</details>

## Scheduling tasks

As with any other workflow tool, scheduling tasks make up part of Prefect's core feature set. In the example below, we add a `Schedule` to our `Flow` and have configured the task to run at 2 minute intervals. We have also configured our schedule with a specific start date (1 minute after triggering the run) and end date (5 minutes after triggering the run) and have used the [pendulum](https://pendulum.eustace.io/) library to make working with dates and times easier.

Finally, the timezone is set to the systems timezone as the standard `datetime.now()` method in the `start_date` parameter mean that the schedule is set to UTC and your flow many not run when you expect it to run.

```python
# schedule.py
from prefect import task, Flow
from prefect.schedules import Schedule
from prefect.schedules.clocks import IntervalClock
import pendulum

now = pendulum.now("Pacific/Auckland")
start = now.add(minutes=1)
end = now.add(minutes=5)

schedule = Schedule(
    clocks=[IntervalClock(pendulum.duration(minutes=2), start_date=start, end_date=end)]
)

@task(log_stdout=True)
def print_current_datetime():
    print("The current datetime is: {}.".format(str(pendulum.now())))

with Flow("Schedule-Flow", schedule=schedule) as flow:
    print_current_datetime()

flow.run()
```

<details>
  <summary>Show terminal output</summary>

```
user@machine:~$ python schedule.py
[2020-06-28 10:04:23,137] INFO - prefect.Flow: Schedule-Flow | Waiting for next scheduled run at 2020-06-28T22:05:23.132497+12:00
[2020-06-28 10:05:23,191] INFO - prefect.FlowRunner | Beginning Flow run for 'Schedule-Flow'
[2020-06-28 10:05:23,197] INFO - prefect.FlowRunner | Starting flow run.
[2020-06-28 10:05:23,220] INFO - prefect.TaskRunner | Task 'print_current_datetime': Starting task run...
[2020-06-28 10:05:23,227] INFO - prefect.TaskRunner | The current datetime is: 2020-06-28T22:05:23.226899+12:00.
[2020-06-28 10:05:23,236] INFO - prefect.TaskRunner | Task 'print_current_datetime': finished task run for task with final state: 'Success'
[2020-06-28 10:05:23,236] INFO - prefect.FlowRunner | Flow run SUCCESS: all reference tasks succeeded
[2020-06-28 10:05:23,237] INFO - prefect.Flow: Schedule-Flow | Waiting for next scheduled run at 2020-06-28T22:07:23.132497+12:00
[2020-06-28 10:07:23,134] INFO - prefect.FlowRunner | Beginning Flow run for 'Schedule-Flow'
[2020-06-28 10:07:23,143] INFO - prefect.FlowRunner | Starting flow run.
[2020-06-28 10:07:23,162] INFO - prefect.TaskRunner | Task 'print_current_datetime': Starting task run...
[2020-06-28 10:07:23,163] INFO - prefect.TaskRunner | The current datetime is: 2020-06-28T22:07:23.162952+12:00.
[2020-06-28 10:07:23,170] INFO - prefect.TaskRunner | Task 'print_current_datetime': finished task run for task with final state: 'Success'
[2020-06-28 10:07:23,171] INFO - prefect.FlowRunner | Flow run SUCCESS: all reference tasks succeeded
[2020-06-28 10:07:23,171] INFO - prefect.Flow: Schedule-Flow | Waiting for next scheduled run at 2020-06-28T22:09:23.132497+12:00
[2020-06-28 10:09:23,197] INFO - prefect.FlowRunner | Beginning Flow run for 'Schedule-Flow'
[2020-06-28 10:09:23,209] INFO - prefect.FlowRunner | Starting flow run.
[2020-06-28 10:09:23,246] INFO - prefect.TaskRunner | Task 'print_current_datetime': Starting task run...
[2020-06-28 10:09:23,246] INFO - prefect.TaskRunner | The current datetime is: 2020-06-28T22:09:23.246765+12:00.
[2020-06-28 10:09:23,254] INFO - prefect.TaskRunner | Task 'print_current_datetime': finished task run for task with final state: 'Success'
[2020-06-28 10:09:23,254] INFO - prefect.FlowRunner | Flow run SUCCESS: all reference tasks succeeded
```

</details>

From here, you may want to expand on [handling task failures](https://docs.prefect.io/core/tutorial/04-handling-failure.html) or continue with setting up "orchestration"/Prefect Core server which has a handy UI for visualising all your flows and jobs - this can come in handy if you have many flows and tasks that have or are dependencies and those that are executed asynchronously.

## Prefect Core server

To be continued...
